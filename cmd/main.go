package main

import (
	"context"
	"douyin-ecommerce/config"
	"fmt"
	"github.com/cloudwego/hertz/pkg/app"
	"github.com/cloudwego/hertz/pkg/app/server"
	"log"
)

func main() {
	// 初始化数据库和中间件
	config.InitMySQL()
	config.InitRedis()
	config.InitElasticSearch()
	config.InitMilvus()

	// 启动Hertz服务
	h := server.Default()

	h.GET("/", func(c context.Context, ctx *app.RequestContext) {
		ctx.String(200, "Welcome to Douyin E-commerce!")
	})

	// 使用 Run 启动服务
	// 使用 Run 启动服务
	err := h.Run()
	if err != nil {
		log.Fatalf("Hertz server failed: %v", err)
	}

	fmt.Println("Server is running on http://localhost:8080")
}
