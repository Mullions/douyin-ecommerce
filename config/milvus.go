package config

import (
	"context"
	"fmt"
	"github.com/milvus-io/milvus-sdk-go/v2/client"
	"log"
)

// MilvusClient 全局 Milvus 客户端
var MilvusClient client.Client

// InitMilvus 初始化 Milvus 客户端
func InitMilvus() {
	var err error
	MilvusClient, err = client.NewClient(context.Background(), client.Config{
		Address: "localhost:19530", // Milvus 服务地址
	})
	if err != nil {
		log.Fatalf("Milvus connection failed: %v", err)
	}
	fmt.Println("Milvus connected successfully")
}
