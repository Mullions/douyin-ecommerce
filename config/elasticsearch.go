package config

import (
	"fmt"
	"github.com/elastic/go-elasticsearch/v7"
	"log"
)

// EsClient 全局 Elasticsearch 客户端
var EsClient *elasticsearch.Client

// InitElasticSearch 初始化 Elasticsearch 客户端
func InitElasticSearch() {
	var err error
	EsClient, err = elasticsearch.NewClient(elasticsearch.Config{
		Addresses: []string{
			"http://localhost:9200", // Elasticsearch 地址
		},
	})
	if err != nil {
		log.Fatalf("Error creating the client: %s", err)
	}
	fmt.Println("ElasticSearch connected successfully")
}
