package config

import (
	"database/sql"
	"fmt"
	"log"

	_ "github.com/go-sql-driver/mysql"
)

var DB *sql.DB

// InitMySQL 初始化 MySQL 连接
func InitMySQL() {
	var err error
	dsn := "user:password@tcp(127.0.0.1:3306)/douyin_ecommerce?charset=utf8mb4&parseTime=True&loc=Local"
	DB, err = sql.Open("mysql", dsn)
	if err != nil {
		log.Fatalf("MySQL connection failed: %v", err)
	}

	err = DB.Ping()
	if err != nil {
		log.Fatalf("MySQL Ping failed: %v", err)
	}

	fmt.Println("MySQL connected successfully")
}
